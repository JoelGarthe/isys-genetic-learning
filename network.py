
import os
#os.environ["CUDA_VISIBLE_DEVICES"]="-1"    
import tensorflow as tf
from KerasGA import GeneticAlgorithm
import pygad.kerasga
import pygad.cnn
import tensorflow as tf
import tensorflow.keras
import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, Flatten, BatchNormalization, Dropout
from doodlejump import DoodleJump
from tensorflow.keras.activations import softmax
from tensorflow.keras import layers, models, optimizers
from tensorflow.keras.layers import  ConvLSTM2D, TimeDistributed

import tensorflow.keras
from tensorflow.keras.layers import Dense, LSTM, \
    Flatten, TimeDistributed, Conv2D, Dropout
from tensorflow.keras import Sequential
from tensorflow.keras.applications.vgg16 import VGG16


from datetime import datetime
from matplotlib import pyplot
from matplotlib.animation import FuncAnimation
from random import randrange

game = DoodleJump(seed=2)
tf.executing_eagerly()


# The fitness function is one of the functions that can be split into multiple threads
# Make use of that!

fitness_array = []

# Fitness function that KerasGa will use to evaluate the networks
def fitness_func(solution, sol_idx):
    global data_inputs, data_outputs, keras_ga, model, fitness_array

    #if(sol_idx):


    #    if len(fitness_array) -1 >= sol_idx:
    #        return fitness_array[sol_idx]

    # Create a matrix that can be used with keras from the previously created weight values
    model_weights_matrix = pygad.kerasga.model_weights_as_matrix(model=model, weights_vector=solution)

    # Pass the weights to the model
    model.set_weights(weights=model_weights_matrix)

    # Get predictions based on the inputs
    #predictions = model.predict(data_inputs)

    #print("run loop")

    while game.isGameOver() == False:

        # Make the game run 20 ticks
        game.run(ticks = 20)

        #print("run loop")

        # Retrieve the current gamestate
        gameState = game.getCurrentGameState()

        #print(gameState)

        # Get a value based on the current gamestate
        predictions = model.predict(gameState)

        #print(predictions)

        #print(predictions)

        # Pass the input from the model to the game
        game.act(predictions)


    #print("Finished Solution #" + str(sol_idx))
    score = game.currentScore()

    fitness_array.append(score)

    game.resetGame()
    game.initGame()

    return score



# x_data, y_data = [], []

# figure = pyplot.figure()
# line, = pyplot.plot_date(x_data, y_data, '-')

# def update(frame):
#     line.set_data(x_data, y_data)
#     figure.gca().relim()
#     figure.gca().autoscale_view()
#     return line,

# animation = FuncAnimation(figure, update, interval=200)

# pyplot.show()

# Callback function for a lifecycle event
# Used for simple text output
def callback_generation(ga_instance):
    global game, fitness_array

    #print(ga_instance.generations_completed )

    #x_data.append(ga_instance.generations_completed)
    #y_data.append(ga_instance.best_solution()[1])

    fitness_array = [] 

    print("Generation = {generation} Fitness = {fitness}".format(generation=ga_instance.generations_completed, fitness=ga_instance.best_solution()[1]))
    filename = 'genetic'

    

    ga_instance.save(filename=filename)



# Build up the keras model 
# model = tf.keras.models.Sequential([
#     Conv2D(64, kernel_size=3, activation="relu", input_shape=(40, 30, 1)),
#     Conv2D(32, kernel_size=3, activation="sigmoid"),
#     Flatten(),
#     Dense(128),
#     Dense(64),
#     Dense(3)
# ])



model = Sequential()

# Build the keras model using the functional API.
input_layer = tensorflow.keras.layers.Input(shape=(224, 224, 3))



base_model = tf.keras.applications.MobileNetV3Small(
    input_shape=(224, 224, 3), 
    include_top=True, 
    weights='imagenet', 
    pooling='avg'
    )

base_model.trainable = False


# model = tf.keras.Sequential([
#     base_model,
#     tensorflow.keras.layers.Dense(1, activation="sigmoid")
# ])

model = tf.keras.Sequential([
    base_model,
    #tensorflow.keras.layers.Dense(18),
    tensorflow.keras.layers.Dense(3, activation="softmax")
])


#model = tf.keras.models.load_model("./pretrained.h5")


#layers.Dense(1024, activation='relu', name='hidden_layer'),

print(model.summary())


# num_solutions = Number of Networks in the each population
keras_ga = pygad.kerasga.KerasGA(model=model,
                                 num_solutions=50)

def on_fitness(ga_instance, population_fitness):
    print("Durchschnitt: " + str(sum(population_fitness) / len(population_fitness)))

def on_start(ga_instance):
    pass#print("on_start()")

def on_parents(ga_instance, selected_parents):
    pass#print("on_parents()")

def on_crossover(ga_instance, offspring_crossover):
    pass#print("on_crossover()")

def on_mutation(ga_instance, offspring_mutation):
    pass#print("on_mutation()")

def on_stop(ga_instance, last_population_fitness):
    pass#print("on_stop()")


# ga_instance = pygad.GA(num_generations=num_generations, 
#                        num_parents_mating=num_parents_mating, 
#                        initial_population=initial_population,
#                        fitness_func=fitness_func,
#                        mutation_percent_genes=0.01,
#                        mutation_type="random",
#                        init_range_low=0.4,
#                        init_range_high=0.6,
#                        mutation_by_replacement=False,
#                        random_mutation_min_val=0.2,
#                        random_mutation_max_val=0.8,
#                        on_generation=callback_generation)

# Prepare the PyGAD parameters. Check the documentation for more information: https://pygad.readthedocs.io/en/latest/README_pygad_ReadTheDocs.html#pygad-ga-class
num_generations = 300 # Number of generations.
num_parents_mating = 6 # Number of solutions to be selected as parents in the mating pool.
initial_population = keras_ga.population_weights # Initial population of network weights
parent_selection_type = "rank" # Type of parent selection.
crossover_type = "single_point" # Type of the crossover operator.
mutation_type = "adaptive" # Type of the mutation operator.

mutation_probability = [0.25, 0.15]

mutation_percent_genes = [15, 5] # Percentage of genes to mutate. This parameter has no action if the parameter mutation_num_genes exists.
keep_parents = 1 # Number of parents to keep in the next population. -1 means keep all parents and 0 means keep nothing.



ga_instance = pygad.GA(num_generations=num_generations,
                       num_parents_mating=num_parents_mating,
                       initial_population=initial_population,
                       fitness_func=fitness_func,
                       parent_selection_type=parent_selection_type,
                       crossover_type=crossover_type,
                       mutation_type=mutation_type,
                       mutation_percent_genes=mutation_percent_genes,
                       keep_parents=keep_parents,
                       on_generation=callback_generation,
                       on_fitness=on_fitness,
                       on_start=on_start,
                       mutation_probability=mutation_probability,
                       on_parents=on_parents,
                       on_crossover=on_crossover,
                       on_mutation=on_mutation,
                       on_stop=on_stop
                       )


# ga_instance = pygad.load('alsoDumbButGoodBoi')

solution, solution_fitness, solution_idx = ga_instance.best_solution()

print(solution_fitness)

ga_instance.run()

# Create a visual plot 
ga_instance.plot_result(title="Doodle AI - Iteration vs. Fitness", linewidth=4)


solution, solution_fitness, solution_idx = ga_instance.best_solution()
print("Parameters of the best solution : {solution}".format(solution=solution))
print("Fitness value of the best solution = {solution_fitness}".format(solution_fitness=solution_fitness))
print("Index of the best solution : {solution_idx}".format(solution_idx=solution_idx))

if ga_instance.best_solution_generation != -1:
    print("Best fitness value reached after {best_solution_generation} generations.".format(best_solution_generation=ga_instance.best_solution_generation))
    