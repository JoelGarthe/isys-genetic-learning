from typing import List
import pygame
from pygame.locals import *
import sys
import random
random.seed(2)


import matplotlib.pyplot as plt
import matplotlib.image as mpimg

import skimage.color
import skimage.io
import numpy as np
import tensorflow as tf 
from numpy.random import choice


from PIL import Image
import PIL



class DoodleJump:
    screenHeight = 600
    timeoutLimit = 150
    rowDistance = (screenHeight/10)
    survivedTicks = 0
    image_list = []
    labeled_list = []
    index = 6

    def act(self, input):
        '''
        Take in controls, return the current gamestate
        '''

  

        randomElement = choice(range(len(input[0])), p=input[0])

        # Map input number to keypresses
        # Seperation of left and right may make it easier for the ai 
        if randomElement == 0:
            self.simulatedKeypressLeft = True
            self.simulatedKeypressRight = False
            
        if randomElement == 2:
            self.simulatedKeypressLeft = False
            self.simulatedKeypressRight = True

        if randomElement == 1:
            self.simulatedKeypressLeft = False
            self.simulatedKeypressRight = False


    def isGameOver(self):
        '''
        Check if a game over occured
        '''
        return self.gameIsOver

    def currentScore(self):
        self.score += self.survivedTicks/2
        return self.score
        
    def addCurrentGameState(self):
        
        string_image = pygame.image.tostring(self.screen, 'RGB')
        temp_surf = pygame.image.fromstring(string_image,(800, self.screenHeight),'RGB' )
        tmp_arr = pygame.surfarray.array3d(temp_surf)
        
        
        img = Image.fromarray(tmp_arr, 'RGB')

        # img.save('mybefore.png')
        # img.show()

        img = img.resize((224, 224), PIL.Image.ANTIALIAS).convert('RGB')
        
        
        image_as_numpy = np.array(img)

        #print(image_as_numpy.shape)
        

        image_after_reshape = tf.reshape(image_as_numpy, (1, 224, 224, 3))  
        if self.direction != None:
            self.image_list.append(image_after_reshape)
            self.labeled_list.append(self.direction)
            

        return image_after_reshape



    def resetGame(self):
        self.gameIsOver = False
        self.cameray = 0
        self.score = 0
        self.survivedTicks = 0
        self.springs = []
        self.platforms = [[400, 500, 0, 0]]
        #self.generatePlatforms()
        self.playerx = 400
        self.playery = 400

    def gameOver(self):
        np.save("./games/game_images_{}.npy".format(self.index), self.image_list[:-35], allow_pickle=True)
        np.save("./games/game_labels_{}.npy".format(self.index), self.labeled_list[:-35], allow_pickle=True)
        self.gameIsOver = True

    def __init__(self):
        self.screen = pygame.display.set_mode((800, 600))
        self.green = pygame.image.load("assets/green.png").convert_alpha()
        pygame.font.init()
        self.score = 0
        self.survivedTicks = 0
        self.font = pygame.font.SysFont("Arial", 25)
        self.blue = pygame.image.load("assets/blue.png").convert_alpha()
        self.red = pygame.image.load("assets/red.png").convert_alpha()
        self.red_1 = pygame.image.load("assets/red_1.png").convert_alpha()
        self.playerRight = pygame.image.load("assets/right.png").convert_alpha()
        self.playerRight_1 = pygame.image.load("assets/right_1.png").convert_alpha()
        self.playerLeft = pygame.image.load("assets/left.png").convert_alpha()
        self.playerLeft_1 = pygame.image.load("assets/left_1.png").convert_alpha()
        self.spring = pygame.image.load("assets/spring.png").convert_alpha()
        self.spring_1 = pygame.image.load("assets/spring_1.png").convert_alpha()
        self.direction = 0
        self.playerx = 0
        self.playery = 0
        #Initialize a Platform at 400,500 from Type normal, (not broken....)
        self.platforms = [[400, 500, 0, 0]]
        self.platforms = []
        self.springs = []
        self.cameray = 0
        self.jump = 0
        self.gravity = 0
        self.xmovement = 0
        self.gameIsOver = False
        self.simulatedKeypressRight = False
        self.simulatedKeypressLeft = False
        self.initGame()
        self.scoreIncreaseFreshnessCounter = DoodleJump.timeoutLimit
    
    def updatePlayer(self):
        if not self.jump:        
            self.playery += self.gravity
            self.gravity += 1
        elif self.jump:
            self.playery -= self.jump
            self.jump -= 1
        key = pygame.key.get_pressed()
        if key[K_RIGHT] or self.simulatedKeypressRight == True:
            if self.xmovement < 10:
                self.xmovement += 1
            self.direction = 0

        elif key[K_LEFT] or self.simulatedKeypressLeft == True:
            if self.xmovement > -10:
                self.xmovement -= 1
            self.direction = 1
        else:
            if self.xmovement > 0:
                self.xmovement -= 1
            elif self.xmovement < 0:
                self.xmovement += 1
            self.direction = None
        if self.playerx > 850:
            self.playerx = -50
        elif self.playerx < -50:
            self.playerx = 850
        self.playerx += self.xmovement
        if self.playery - self.cameray <= 200:
            self.cameray -= 10
        if not self.direction:
            if self.jump:
                self.screen.blit(self.playerRight_1, (self.playerx, self.playery - self.cameray))
            else:
                self.screen.blit(self.playerRight, (self.playerx, self.playery - self.cameray))
        else:
            if self.jump:
                self.screen.blit(self.playerLeft_1, (self.playerx, self.playery - self.cameray))
            else:
                self.screen.blit(self.playerLeft, (self.playerx, self.playery - self.cameray))

    def updatePlatforms(self):
        for p in self.platforms:
            rect = pygame.Rect(p[0], p[1], self.green.get_width() - 10, self.green.get_height())
            player = pygame.Rect(self.playerx, self.playery, self.playerRight.get_width() - 10, self.playerRight.get_height())
            if rect.colliderect(player) and self.gravity and self.playery < (p[1] - self.cameray):
                if p[2] != 2:
                    self.jump = 15
                    self.gravity = 0
                else:
                    p[-1] = 1
                    #Could add single Jump on reds here
            #move blue Platforms between 550 and 0
            if p[2] == 1:
                if p[-1] == 1:
                    p[0] += 5
                    if p[0] > 550:
                        p[-1] = 0
                else:
                    p[0] -= 5
                    if p[0] <= 0:
                        p[-1] = 1

    def drawPlatforms(self):
        for p in self.platforms:
            
            #check the type of the Platform (normal, moving, red) and print the fitting one
            if p[2] == 0:
                self.screen.blit(self.green, (p[0], p[1] - self.cameray))
            elif p[2] == 1:
                self.screen.blit(self.blue, (p[0], p[1] - self.cameray))
            elif p[2] == 2:
                #check if is broken
                if not p[3]:
                    self.screen.blit(self.red, (p[0], p[1] - self.cameray))
                else:
                    self.screen.blit(self.red_1, (p[0], p[1] - self.cameray))

        #Draw springs and check if they are used
        for spring in self.springs:
            if spring[-1]:
                self.screen.blit(self.spring_1, (spring[0], spring[1] - self.cameray))
            else:
                self.screen.blit(self.spring, (spring[0], spring[1] - self.cameray))
            if pygame.Rect(spring[0], spring[1], self.spring.get_width(), self.spring.get_height()).colliderect(pygame.Rect(self.playerx, self.playery, self.playerRight.get_width(), self.playerRight.get_height())):
                
                self.jump = 50
                self.cameray -= 50

    def increaseScore(self, amount):
        self.score = self.score + amount
        self.scoreIncreaseFreshnessCounter = DoodleJump.timeoutLimit

    def updatePlatformRows(self):
        """Create a new row of Platforms when the Cameraview is above the highest platform. Also inreases Score """
        check = self.platforms[0][1] - self.cameray
            #when camera is above highest platform, generate a new row of Platforms
        if check > DoodleJump.screenHeight and random.randint(0, 10) > 8:
            #append the Platform 50 above the prev highest
            self.generatePlatform(self.platforms[-1][1] - 50, True)
            
            #if bottom row is not visible delete lowest platform
            #if (check-self.rowDistance) > DoodleJump.screenHeight:
            self.platforms.pop(0)
            self.increaseScore(100)

    def generatePlatforms(self):
        """Generates all Platforms on the start Screen """
        yPos =  DoodleJump.screenHeight
        while yPos > 0:
            self.generatePlatform(yPos, False)
            yPos -= self.rowDistance

    def generatePlatform(self, yPos, springs):
        """Generate a Platform at a given Height """
        xPos = random.randint(0,700)
        platform = self.generatePlatformType()
        self.platforms.append([xPos, yPos, platform, 0])
        if springs and False:
            withSpring = (random.randint(0, 100) > 90 and platform == 0)
            coords = self.platforms[-1]
            if withSpring:
                self.springs.append([coords[0], coords[1] - 25, 0])

    def generatePlatformType(self):
        """return a random Platform type """
        platform = random.randint(0, 100)
        if platform < 100:
            #normal type  80%
            platform = 0
        elif platform < 90:
            #moving type 10%
            platform = 1
        else:
            #red type 10%
            platform = 2
        return platform

    def drawGrid(self):
        for x in range(80):
            pygame.draw.line(self.screen, (222,222,222), (x * 12, 0), (x * 12, 600))
            pygame.draw.line(self.screen, (222,222,222), (0, x * 12), (800, x * 12))

    def initGame(self):
        self.clock = pygame.time.Clock()
        self.generatePlatforms()
        self.scoreIncreaseFreshnessCounter = DoodleJump.timeoutLimit

    def run(self):


        while not self.gameIsOver:

            self.screen.fill((255,255,255))
            self.clock.tick(60)
            for event in pygame.event.get():
                if event.type == QUIT:
                    sys.exit()
            if self.playery - self.cameray > 700:
                self.gameOver()
            self.drawGrid()
            self.updatePlatformRows()
            self.drawPlatforms()
            self.updatePlayer()
            self.updatePlatforms()
            self.screen.blit(self.font.render(str(self.score), -1, (0, 0, 0)), (25, 25))

            pygame.display.flip() 
            self.addCurrentGameState()
            


DoodleJump().run()
