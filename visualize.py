gameState = None
model = None

import tensorflow as tf 
import numpy as np
import tensorflow.keras.backend as K
import cv2

def visualize(gameState, model):

    print(model.get_layer('conv'))
    with tf.GradientTape() as tape:
        last_conv_layer = model.get_layer('conv')
        iterate = tf.keras.models.Model([model.inputs], [model.output, last_conv_layer.output])
        model_out, last_conv_layer = iterate(gameState)
        class_out = model_out[:, np.argmax(model_out[0])]
        grads = tape.gradient(class_out, last_conv_layer)
        pooled_grads = K.mean(grads, axis=(0, 1, 2))

    heatmap = tf.reduce_mean(tf.multiply(pooled_grads, last_conv_layer), axis=-1)
    heatmap = np.maximum(heatmap, 0)
    heatmap /= np.max(heatmap)
    heatmap = heatmap.reshape((8, 8))

    img = cv2.imread(gameState)

    heatmap = cv2.resize(heatmap, (img.shape[1], img.shape[0]))

    heatmap = cv2.applyColorMap(np.uint8(255*heatmap), cv2.COLORMAP_JET)

    img = heatmap * 0.3 + img

    cv2.imshow(gameState)
    cv2.imshow(img)