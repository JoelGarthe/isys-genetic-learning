
import tensorflow as tf
import tensorflow.keras
import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, Flatten, BatchNormalization, Dropout
from doodlejump import DoodleJump
from tensorflow.keras.activations import softmax
from tensorflow.keras import layers, models, optimizers
from tensorflow.keras.layers import  ConvLSTM2D, TimeDistributed

import tensorflow.keras
from tensorflow.keras.layers import Dense, LSTM, \
    Flatten, TimeDistributed, Conv2D, Dropout
from tensorflow.keras import Sequential
from tensorflow.keras.applications.vgg16 import VGG16

images0, labels0 = np.load('.\games\game_images_0.npy'), np.load('.\games\game_labels_0.npy')
images1, labels1 = np.load('.\games\game_images_1.npy'), np.load('.\games\game_labels_1.npy')
images2, labels2 = np.load('.\games\game_images_2.npy'), np.load('.\games\game_labels_2.npy')
images3, labels3 = np.load('.\games\game_images_3.npy'), np.load('.\games\game_labels_3.npy')
images4, labels4 = np.load('.\games\game_images_4.npy'), np.load('.\games\game_labels_4.npy')

images = np.concatenate((images0, images1 ,images2,images3,images4))
labels = np.concatenate((labels0, labels1 ,labels2, labels3 ,labels4))
model = Sequential()
images = np.squeeze(images,axis=1)
print(np.shape(images),"------------------------------------------------------------------------------")


vgg = tf.keras.applications.MobileNetV3Small(
    include_top=False,
    weights='imagenet', 
    input_shape=(224, 224, 3)
)


vgg.trainable = False
first_dense = Dense(16, activation='relu')

# Build the keras model using the functional API.
# add vgg model for 5 input images (keeping the right shape
model.add(vgg)
model.add(Flatten())
model.add(first_dense)
model.add(Dense(16, activation='relu'))
model.add(Dense(1, activation="sigmoid"))



#layers.Dense(1024, activation='relu', name='hidden_layer'),

print(model.summary())

model.compile(optimizer = "adam", loss = "binary_crossentropy", metrics=['accuracy'])

model.fit(images, labels, epochs=20, batch_size = 256, verbose = True)

first_dense.trainable = False

print(model.summary())

model.save('pretrained.h5')